﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileOperatorManagment.CustomExeptions
{
    public class InvaliRatingException:Exception
    {
        public InvaliRatingException(string message) : base(message)
        {
        }
    }
}
