﻿using System;
using System.Collections;
using System.Collections.Generic;
using MobileOperatorManagment.CustomExeptions;
using MobileOperatorManagment.DataAccessLayer;
using MobileOperatorManagment.Entities;

namespace MobileOperatorManagment.BusinessLayer

{
    public class MobileOperatorBL
    {
        
        MobileOperatorDAL mobileOperatorDA = new MobileOperatorDAL();
        //List<Mobileoperator> mobOperatorLists = new List<Mobileoperator>();

        public  int AddMobileOperator(Mobileoperator mobileOperator)
        {
          
               // MobileOperatorDAL mobileOperatorDA = new MobileOperatorDAL();
                return mobileOperatorDA.AddMobileOperator(mobileOperator);
        }

        public ArrayList DisplayMobileOperatorDetails()
        {
            return mobileOperatorDA.DisplayMobileOperatorDetails();
        }

        public int AddPerson(Person person)
        {
            return mobileOperatorDA.AddPerson(person);
        }

        public ArrayList DisplayTwoMobileOperatorDetails()
        {
            return mobileOperatorDA.DisplayTwoMobileOperatorDetails();
        }

        public string SearchDeatils(Person person)
        {
            return mobileOperatorDA.SearchDeatils(person);
        }

        public ArrayList WriteDataInTextFileBL()
        {
            return mobileOperatorDA.WriteDataInTextFileDAL();
        }
    }
}
