﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileOperatorManagment.Entities
{
    [Serializable]
   public class Mobileoperator
    {
        public int OperatorId { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }
    }
}
