﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileOperatorManagment.Entities
{
    [Serializable]
    public class Person
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public int Operator { get; set; }
    }
}
