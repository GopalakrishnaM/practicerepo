﻿using System;
using MobileOperatorManagment.BusinessLayer;
using System.IO;
using MobileOperatorManagment.CustomExeptions;
using System.Collections.Generic;
using System.Collections;
using MobileOperatorManagment.Entities;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MobileOperatorManagment.PresentationLayer

{
    class MobileOperatorPL
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            Mobileoperator mobileOperator = new Mobileoperator();
            MobileOperatorBL mobileOperatorBL = new MobileOperatorBL();
            bool exitFlag = true;
            int rowAffect;
            do
            {
                Console.WriteLine("1.Add mobile operator");
                Console.WriteLine("2.Display mobile operator with name and Id");
                Console.WriteLine("3.Add person");
                Console.WriteLine("4.Display top Two mobile operator by rating");
                Console.WriteLine("5.Search person details with mobile operator using Id");
                Console.WriteLine("6.Display details to file;");
                Console.WriteLine("Enter Your choice;");
                int option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        Console.WriteLine("Enter Operator Id:");
                        mobileOperator.OperatorId = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter Operator NAme:");
                        mobileOperator.Name = Console.ReadLine();
                        Console.WriteLine("Enter Rating");
                        mobileOperator.Rating = Convert.ToInt32(Console.ReadLine()); 
                        try
                        {


                            rowAffect = mobileOperatorBL.AddMobileOperator(mobileOperator);
                            if (rowAffect > 0)
                            {

                                Console.WriteLine("Data inserted Successfully");
                            }
                        }
                        catch(InvaliRatingException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 2:
                        
                        try
                        {
                            ArrayList arrayList = mobileOperatorBL.DisplayMobileOperatorDetails();
                            foreach (var items in arrayList)
                            {
                                Console.WriteLine(items);
                            }
                        }
                        catch(OperatorNotpresent ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 3:
                        Console.WriteLine("Enter Person Id:");
                        person.PersonId = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter Person NAme:");
                        person.PersonName = Console.ReadLine();
                        Console.WriteLine("Enter Operator Id");
                        person.Operator = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            rowAffect = mobileOperatorBL.AddPerson(person);
                            if (rowAffect > 0)
                            {
                                Console.WriteLine("Data inserted Successfully.");
                            }
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 4:
                        ArrayList topTwooperator = new ArrayList();
                        
                            topTwooperator = mobileOperatorBL.DisplayTwoMobileOperatorDetails();
                            foreach (var operators in topTwooperator)
                            {
                                Console.WriteLine(operators);
                            }
                     
                        break;
                    case 5:
                        string details = " ";
                        Console.Write("Enter PersonID:");
                        try {
                            person.PersonId = int.Parse(Console.ReadLine());
                            details = mobileOperatorBL.SearchDeatils(person);
                            if (details != "")
                                Console.WriteLine(details);
                            else Console.WriteLine($"{person.PersonId } is not avilable in database");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 6:
                        try
                        {
                            ArrayList personDetails = mobileOperatorBL.WriteDataInTextFileBL();
                            string path = @"D:\DotNet.txt";
                            IFormatter formatter = new BinaryFormatter();
                            Stream stream = new FileStream(path, FileMode.Open, FileAccess.Write);

                             if (File.Exists(path))
                             {
                            using (stream)//StreamWriter writer = File.AppendText(path)
                                {
                                foreach (var values in personDetails)
                                 {
                                    formatter.Serialize(stream, values);
                                    //writer.WriteLine(values);

                                 }
                                
                               stream.Close();
                                //writer.Close();
                                Console.WriteLine("Details are copied into file in serialized format");
                                    Console.WriteLine("Do u want to print oringinal data(Y/N)");
                                    //Deserialize(pe)
                                    //char value = char.Parse(Console.ReadLine().ToUpper());
                                    //if (value == 'Y')
                                    //{
                                    //    FileStream fileStream = new FileStream(@"D:\DotNet.txt",FileMode.Open);
                                    //    BinaryFormatter binaryFormatter = new BinaryFormatter();
                                    //   string data = " ";
                                    //    data = (string)binaryFormatter.Deserialize(fileStream);
                                    //    Console.WriteLine("original data is..");
                                    //    foreach (var values in data)
                                    //    {
                                    //     // binaryFormatter.Deserialize(fileStream);
                                    //        Console.WriteLine(values);

                                    //    }
                                    //}
                                }
                             }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    default:
                        Console.WriteLine("Enter option From menu");
                        exitFlag = false;
                        break;
                }
            } while (exitFlag);
            
        }
    }
}
