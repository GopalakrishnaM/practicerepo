﻿
using System.Data.SqlClient;
using MobileOperatorManagment.CustomExeptions;
using System.Collections;
using System;
using MobileOperatorManagment.Entities;

namespace MobileOperatorManagment.DataAccessLayer
{
    public class MobileOperatorDAL
    {
        //private static string conString = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;

        //SqlConnection connection = new SqlConnection(conString);
        DBOperation databaseOperation = new DBOperation();
         //List<Mobileoperator> mobileOperatorlists= new List<Mobileoperator>();
        ArrayList arrayList = new ArrayList();

        public  int AddMobileOperator(Mobileoperator mobileOperator)
        {
            //databaseOperation.OpenConnection();
            //connection.Open();
            int rowAffect=0;

                if (FetchAllOperateName(mobileOperator.Name) && ValidateRating(mobileOperator.Rating))
                {
                    databaseOperation.OpenConnection();
                    //connection.Open();

                    SqlCommand cmd = new SqlCommand("Insert into mobile_operator values('" + mobileOperator.OperatorId + "', '" + mobileOperator.Name + "', '" + mobileOperator.Rating + "')", databaseOperation.GetConnection());// connection);

                    rowAffect = cmd.ExecuteNonQuery();

                    databaseOperation.CloseConnection();
                    //connection.Close();

                }
                else
                {
                throw new InvalidExeption("Cannot insert Data");
                }
            
           
            return rowAffect;
        }

        public ArrayList WriteDataInTextFileDAL()
        {
            
            databaseOperation.OpenConnection() ;
            SqlCommand command = new SqlCommand("SELECT Person.person_id, mobile_operator.Operator_id, Person.person_name, mobile_operator.Operator_Name FROM Person INNER JOIN mobile_operator ON mobile_operator.Operator_Id = Person.operator_id;", databaseOperation.GetConnection());
            var reader = command.ExecuteReader();
            while (reader.HasRows)
            {
                while (reader.Read())
                {
                   arrayList.Add($"PersonId :{reader[0]}  PersonName :{reader[1]} MobileOperatorName :{reader[2]} ");
                }
                reader.NextResult(); 
            }
            reader.Close();
            databaseOperation.CloseConnection();
            return arrayList;
        }

        public string SearchDeatils(Person person)
        {
            databaseOperation.OpenConnection();

            SqlCommand command = new SqlCommand("SELECT Person.person_id, mobile_operator.Operator_id, Person.person_name, mobile_operator.Operator_Name, mobile_operator.rating FROM Person INNER JOIN mobile_operator ON mobile_operator.Operator_Id = Person.operator_id where Person.person_id = '" + person.PersonId + "'", databaseOperation.GetConnection());
            
            string rowsAffected = "";
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                rowsAffected = $"PersonId :{reader[0]} MobileOperatorId:{reader[1]} PersonName :{reader[2]} MobileOperatorName :{reader[3]} MobileOperatorRating :{reader[4]}";
            }
            reader.Close();
            databaseOperation.CloseConnection(); 


            return rowsAffected;

        }

        public ArrayList DisplayTwoMobileOperatorDetails()
        {
            arrayList.Clear();
            databaseOperation.OpenConnection();
            SqlCommand command = new SqlCommand("select*from GetTopTwoOperator()", databaseOperation.GetConnection());
            var reader = command.ExecuteReader();
            
            //while (reader.HasRows)
            //{
                while (reader.Read())
                {
                    arrayList.Add($"OperatorName:{reader[0]} MobileOperatorRating :{reader[1]}");
                }
            //}
            reader.Close();
            databaseOperation.CloseConnection();
            return arrayList;
        }

        public int AddPerson(Person person)
        {

            databaseOperation.OpenConnection();
            SqlCommand sqlcom = new SqlCommand("INSERT INTO Person VALUES ('" + person.PersonId + "','" + person.PersonName + "', '" + person.Operator + "')", databaseOperation.GetConnection());

            int rowsAffected = sqlcom.ExecuteNonQuery(); 

            databaseOperation.CloseConnection();
            return rowsAffected;
        }

        public ArrayList DisplayMobileOperatorDetails()
        {
            arrayList.Clear();
            databaseOperation.OpenConnection();
            SqlCommand cmd = new SqlCommand("Select * from mobile_operator", databaseOperation.GetConnection());
            var reader = cmd.ExecuteReader();

            if (!reader.HasRows)
            {

                throw new OperatorNotpresent("Operator details Not prsent");
            }
            while (reader.Read())
            {
               arrayList.Add($"OperatorId :{reader[0]}  OperatorName :{reader[1]} OperatorRating :{reader[2]} ");
                           
            }
            reader.Close();
            return arrayList;
        }
            
        private bool ValidateRating(int rating)
        {

            if (rating <= 5)
            {

                return true;
            }
            else
            {

                throw new InvaliRatingException("Rating should be less than or equal to 5");
            }
        }
        public bool FetchAllOperateName(string mobileOperator)
        {
            databaseOperation.OpenConnection();
            //connection.Open();

            // SqlCommand cmd = new SqlCommand("Select * from mobile_operator where Operator_Name = '" + mobileOperator + "'", databaseOperation.GetConnection());// connection);

            SqlCommand cmd = new SqlCommand("select*from GetMobileOperator('"+mobileOperator+"')", databaseOperation.GetConnection());
            var dataReader = cmd.ExecuteReader();
            bool result;
            if (!dataReader.HasRows)
            {
               result=true;
            }
            else
            {
               
                throw new InvaliOperatorException("Operator Name Shouldnot REapeted");

            }
            databaseOperation.CloseConnection();
            return result;
            //connection.Close();
        }


    }
}