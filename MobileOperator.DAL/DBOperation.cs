﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileOperatorManagment.DataAccessLayer
{
    public class DBOperation
    {
        
        private static string conString = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;

        SqlConnection connection = new SqlConnection(conString);
        public SqlConnection GetConnection()
        {
            return connection;
        }
        public void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }
}
